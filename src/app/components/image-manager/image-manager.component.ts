import { Component, Input, OnInit } from '@angular/core';
import { IImageManager } from './image-manager.interface';


@Component({
    selector: 'image-manager',
    templateUrl: './image-manager.component.html',
    styleUrls: ['./image-manager.component.scss']
})
export class ImageManagerComponent implements OnInit {
    @Input() images: IImageManager[] = []
    @Input() icon: boolean;

    constructor() { }

    ngOnInit(): void {
    }

}
