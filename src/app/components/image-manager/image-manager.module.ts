import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageManagerComponent } from './image-manager.component';



@NgModule({
    declarations: [ImageManagerComponent],
    exports: [ImageManagerComponent],
    imports: [
        CommonModule
    ]
})
export class ImageManagerModule { }
