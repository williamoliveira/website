export interface IImageManager {
    title?: string;
    footer?: string;
    src: string;
    description?: string;
}
