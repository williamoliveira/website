import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarModule } from './sidebar/sidebar.module';
import { HeaderModule } from './header/header.module';
import { ImageManagerModule } from './image-manager/image-manager.module';



@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        SidebarModule,
        HeaderModule,
        ImageManagerModule
    ]
})
export class ComponentsModule { }
