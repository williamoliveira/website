import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppsComponent } from './pages/apps/apps.component';
import { GameComponent } from './pages/game/game.component';
import { HomeComponent } from './pages/home/home.component';
import { LibraryComponent } from './pages/library/library.component';
import { SitesComponent } from './pages/sites/sites.component';
import { StrategyComponent } from './pages/strategy/strategy.component';
import { TechnologiesComponent } from './pages/technologies/technologies.component';

const routes: Routes = [

    { path: '', redirectTo: 'home', pathMatch: 'full' },
    // { path: '', redirectTo: 'template', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'strategy', component: StrategyComponent },
    { path: 'sites', component: SitesComponent },
    { path: 'apps', component: AppsComponent },
    { path: 'library', component: LibraryComponent },
    { path: 'game', component: GameComponent },
    { path: 'technologies', component: TechnologiesComponent },
    { path: '**', component: AppComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
