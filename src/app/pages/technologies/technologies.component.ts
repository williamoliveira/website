import { Component, OnInit } from '@angular/core';
import { IImageManager } from 'src/app/components/image-manager/image-manager.interface';

@Component({
    selector: 'app-technologies',
    templateUrl: './technologies.component.html',
    styleUrls: ['./technologies.component.scss']
})
export class TechnologiesComponent implements OnInit {
    sites: IImageManager[] = [
        {
            src: 'tools/angularjs.png',
            description: 'Angular'
        },
        {
            src: 'tools/babel.png',
            description: 'Babel'
        },
        {
            src: 'tools/eslint.png',
            description: 'Eslint'
        },
        {
            src: 'tools/webpack.png',
            description: 'Webpack'
        },
        {
            src: 'tools/html5.jpg',
            description: 'HTML5'
        },
        {
            src: 'tools/javascript.jpg',
            description: 'Javascript'
        },
        {
            src: 'tools/css.jpg',
            description: 'CSS'
        },
        {
            src: 'tools/bootstrap.jpg',
            description: 'Bootstrap'
        },
        {
            src: 'tools/gulp.jpg',
            description: 'Gulp'
        },
        {
            src: 'tools/sass.jpg',
            description: 'SASS'
        },
        {
            src: 'tools/wordpress.jpg',
            description: 'Wordpress'
        },
        {
            src: 'tools/ajax.jpg',
            description: 'Ajax'
        },
    ]

    constructor() { }

    ngOnInit(): void {
    }

}
