import { Component, OnInit } from '@angular/core';
import { IImageManager } from 'src/app/components/image-manager/image-manager.interface';

@Component({
    selector: 'app-apps',
    templateUrl: './apps.component.html',
    styleUrls: ['./apps.component.scss']
})
export class AppsComponent implements OnInit {

    sites: IImageManager[] = [
        {
            src: 'portfolioapps/clicamed.png',
            title: 'ClicaMed Pacientes',
            description: 'Tudo o que você precisa para seu atendimento médico na palma da mão',
            footer: 'Desenvolvido como programador pela Helpper'
        },
        {
            src: 'portfolioapps/clicamed-medicos.png',
            title: 'ClicaMed Médicos',
            description: 'Preencha seus horários da agenda e aumente a rotatividade da sua clínica com flexibilidade e segurança.',
            footer: 'Desenvolvido como programador pela Helpper'
        },
        {
            src: 'portfolioapps/dsfront.png',
            title: 'DsFront dinheiro sem fronteiras',
            description: 'DSFront é Dinheiro sem Fronteira. Desbrave novos caminhos com a segurança de quem sabe aonde quer chegar. A maneira mais fácil de investir em moedas digitais.',
            footer: 'Desenvolvido como programador pela Helpper'
        },
    ]

    constructor() { }

    ngOnInit(): void {
    }

}
