import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home/home.component';
import { StrategyComponent } from './strategy/strategy.component';
import { SitesComponent } from './sites/sites.component';
import { AppsComponent } from './apps/apps.component';
import { ImageManagerModule } from '../components/image-manager/image-manager.module';
import { LibraryComponent } from './library/library.component';
import { GameComponent } from './game/game.component';
import { TechnologiesComponent } from './technologies/technologies.component';


@NgModule({
    declarations: [HomeComponent, StrategyComponent, SitesComponent, AppsComponent, LibraryComponent, GameComponent, TechnologiesComponent],
    imports: [
        CommonModule,
        ImageManagerModule
    ]
})
export class PagesModule { }
