import { Component, OnInit } from '@angular/core';
import { IImageManager } from 'src/app/components/image-manager/image-manager.interface';

@Component({
    selector: 'app-sites',
    templateUrl: './sites.component.html',
    styleUrls: ['./sites.component.scss']
})
export class SitesComponent implements OnInit {

    constructor() { }

    sites: IImageManager[] = [
        {
            src: 'portifolio/texasflood-home.jpg',
            title: 'Texas Flood Banda',
            description: 'Banda de Country Rock atuando em sorocaba e região',
            footer: 'Desenvolvido como freelancer'
        },
        {
            src: 'portifolio/bandeiras-home.jpg',
            title: 'Bandeiras Centro Empresarial',
            description: 'Grande empresa do setor de Galpões',
            footer: 'Desenvolvido como programador pela agência EagleX'
        },
        {
            src: 'portifolio/bm-home.jpg',
            title: 'BM Corporate',
            description: 'Empresa de investimentos',
            footer: 'Desenvolvido como programador pela agência EagleX'
        },
        {
            src: 'portifolio/exponoivas-sorocaba.jpg',
            title: 'Expo Noivas Sorocaba',
            description: 'Evento de casamento',
            footer: 'Desenvolvido como programador pela agência EagleX'
        },
        {
            src: 'portifolio/fryo-home.jpg',
            title: 'Fryo Climatizadores',
            description: 'Grande empresa do setor de climatização em geral',
            footer: 'Desenvolvido como programador pela agência EagleX'
        },
        {
            src: 'portifolio/unniao-home.jpg',
            title: 'Unnião Investimentos',
            description: 'Empresa de investimentos',
            footer: 'Desenvolvido como programador pela agência EagleX'
        },
    ]

    ngOnInit(): void {
    }

}
