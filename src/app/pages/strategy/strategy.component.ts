import { Component, OnInit } from '@angular/core';
import { ClienteStrategy } from './strategy.pattern.example';

@Component({
    selector: 'app-strategy',
    templateUrl: './strategy.component.html',
    styleUrls: ['./strategy.component.css']
})
export class StrategyComponent implements OnInit {

    constructor() { }

    ngOnInit(): void {
        new ClienteStrategy();
    }

}
