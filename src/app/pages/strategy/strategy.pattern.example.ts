
/**
 * Define behavior
 */
interface IDuck {
    swin();
    execFly();
    execQuack();
}
/**
 * Define abstract class Duck
 */
abstract class Duck implements IDuck {
    refFly: IFly;
    refQuack: IQuack;
    swin() {
        console.log("A duck swims");
    }
    actions() {
        this.swin()
        this.execFly()
        this.execQuack()
        console.log('=====\n');
    }
    execFly() {
        this.refFly.fly();
    }
    execQuack() {
        this.refQuack.quack();
    }
}
/**
 * Define behavior Fly
 */
interface IFly { fly(); }
class Flying implements IFly { fly() { console.log("I'm a flyer"); } }
class NFlying implements IFly { fly() { console.log("I don't want to fly ..."); } }
/**
 * Define behavior Quack
 */
interface IQuack { quack(); }
class DoQuack implements IQuack { quack() { console.log("do Quack"); } }
class NQuack implements IQuack { quack() { console.log("in silence..."); } }
/**
 * wild duck
 * Here we have the behavior of flying and making encapsulated quack
 */
class WildDuck extends Duck {
    constructor() {
        super();
        this.refFly = new Flying();
        this.refQuack = new DoQuack();
    }
}
class RubberDuck extends Duck {
    constructor() {
        super();
        this.refFly = new NFlying();
        this.refQuack = new DoQuack();
    }
}
class DecoyDuck extends Duck {
    constructor() {
        super();
        this.refFly = new NFlying();
        this.refQuack = new NQuack();
    }
}
/**
 * whistle
 * 
 * It's not a duck, but it can reuse your behavior
 */
class Whistle {
    refQuack: IQuack;
    constructor() {
        this.refQuack = new DoQuack();
    }
    execQuack() {
        this.refQuack.quack();
    }
}


export class ClienteStrategy {
    constructor() {
        const wildDuck = new WildDuck();
        wildDuck.actions();
        //
        const rubberDuck = new RubberDuck();
        rubberDuck.actions();
        //
        const decoyDuck = new DecoyDuck();
        decoyDuck.actions();
        // It's not a duck :O
        const whistle = new Whistle();
        whistle.execQuack();
    }
}